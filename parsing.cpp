//
// Created by Talha Rehman on 8/25/16.
//

#include "parsing.h"


std::vector<std::string> parsing::regexSplit(const std::string &s, std::string rgx_str) {
    std::vector<std::string> elems;

    std::regex rgx (rgx_str);

    std::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
    std::sregex_token_iterator end;

    while (iter != end)  {
        elems.push_back(*iter);
        ++iter;
    }

    return elems;
}

int parsing::findLine(std::string ocrAddress){

    std::vector<std::string>lines = regexSplit(ocrAddress,"\n");

    std::regex rgx("(.*) ([A-Z][A-Z]) ([0-9-]+)",std::regex_constants::ECMAScript | std::regex_constants::icase );
    std::smatch pieces_match;
    int matched=0;
    int j=0;

    for (int i=0; i<lines.size(); i++) {

        if (std::regex_search(lines[i], pieces_match, rgx)) {
            return i;
        }
    }

}

void parsing::assignValues(std::string line, Address& myAddress){

    std::string arr[3];
    int i = 0;
    std::stringstream ssin(line);

    while (ssin.good() && i < 3){
        ssin >> arr[i];
        ++i;
    }
    myAddress.city=arr[0];
    myAddress.state=arr[1];
    myAddress.zip=arr[2];

}

void parsing::display(Address myaddress){

    std::cout<<"Name: "<<myaddress.name<<std::endl;
    std::cout<<"Location: "<<myaddress.location<<std::endl;
    std::cout<<"City: "<<myaddress.city<<std::endl;
    std::cout<<"State: "<<myaddress.state<<std::endl;
    std::cout<<"Zip: "<<myaddress.zip<<std::endl;
   // std::cout<<"Country: "<<myaddress.country<<std::endl;

}

bool parsing::isNumber(std::string input){

    std::regex rgx("([^\\d]|^)\\d{4,5}([^\\d]|$)");
    std::smatch pieces_match;
    int matched=0;
    int j=0;

    if (std::regex_search(input, pieces_match, rgx)) {
        return 1;
    }

    return 0;
}


bool parsing::parseAddress(std::string rawOcrOutput, Address &parsed){

    std::vector<std::string>lines = regexSplit(rawOcrOutput,"\n");

    int matched;
    std::string location="";

    //assign name feild
    parsed.name=lines[0];

    //find the line containing city, state, zip
    matched=findLine(rawOcrOutput);


    if(matched>6){
        return 0;
    }

    //parse the cityStateZip line to assign City, State, Zip code
    assignValues(lines[matched],parsed);

    if(!(isNumber(parsed.zip))){
        return 0;
    }



    //the lines between cityZipState line and Name line carry location
    for (int i=1; i<matched; i++){
        location=location+lines[i]+" ";
    }



    //assign location feild
    parsed.location=location;


    //check if there is a line after cityStateZip line
    if(lines.size()-1>matched && lines[lines.size()-1].length()<=10 && lines[lines.size()-1]!=""){
        parsed.country = lines[lines.size() - 1];

    }
    else{

        parsed.country="USA";
    }

    //display the struct
    display(parsed);

    return 1;


}


void parsing::test2(std::string rawOCRoutput){
    Address parsed;


    if(!parseAddress(rawOCRoutput,parsed))
        std::cout<<"Test Failed!";


}

void parsing::test(){
    std::string rawOcrOutput="Sam Roberts\n"
            "\n"
            "Pitney Bowes\n"
            "\n"
            "3001 Summer Street\n"
            "Stamford, CT 06905";
    Address parsed;


    if(!parseAddress(rawOcrOutput,parsed))
        std::cout<<"Test Failed!";
}