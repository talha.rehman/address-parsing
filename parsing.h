//
// Created by Talha Rehman on 8/25/16.
//

#ifndef ADDRESSPARSING_PARSING_H
#define ADDRESSPARSING_PARSING_H


#include <iostream>
#include "string"
#include "fstream"
#include <vector>
#include <regex>
#include <sstream>



class parsing {

public:

    struct Address {
        std::string name;
        std::string location;
        std::string city;
        std::string state;
        std::string zip;
        std::string country;
    };


    std::vector<std::string> regexSplit(const std::string &s, std::string rgx_str);
    int findLine(std::string);
    void assignValues(std::string, Address&);
    void display(Address);
    bool parseAddress(std::string rawOcrOutput, Address &parsed);
    bool isNumber(std::string);
    void test();
    void test2(std::string);


};


#endif //ADDRESSPARSING_PARSING_H
